
public class Bus {
private int busNo;
private String route;
private int policeAssigned;


public Bus() {
	super();
}
@Override
public String toString() {
	return "Bus [busNo=" + busNo + ", route=" + route + ", policeAssigned=" + policeAssigned + "]";
}
public Bus(int busNo, String route, int policeAssigned) {
	super();
	this.busNo = busNo;
	this.route = route;
	this.policeAssigned = policeAssigned;
}
public int getBusNo() {
	return busNo;
}
public void setBusNo(int busNo) {
	this.busNo = busNo;
}
public String getRoute() {
	return route;
}
public void setRoute(String route) {
	this.route = route;
}
public int getPoliceAssigned() {
	return policeAssigned;
}
public void setPoliceAssigned(int policeAssigned) {
	this.policeAssigned = policeAssigned;
}


}
