import java.util.List;

public interface IChickenRepository {
	
	boolean add(Chicken item);
	
	List<Chicken> getAll();

}
