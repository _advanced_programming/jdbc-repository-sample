import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InMemoryFoodDb
implements IFoodRepository
{
	private Map<Integer,Food> data;
	
	public InMemoryFoodDb() {
		data = new HashMap<Integer,Food>();
	}

	@Override
	public boolean add(Food item) {
		data.put(item.getId(),item);
		return true;
	}
	
	public Food get(int id) {
		if(data.containsKey(id)) {
			return data.get(id);
		}else {
			return null;
		}
	}

	@Override
	public List<Food> getAll() {
	    List<Food> items = new ArrayList<Food>();
	    for(Food item : data.values()) {
	    	items.add(item);
	    }
		return items;
	}

}
