import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InMemoryChickenDb
implements IChickenRepository
{
	private Map<Integer,Chicken> data;
	
	public InMemoryChickenDb() {
	   data = new HashMap<Integer,Chicken>();	
	}

	@Override
	public boolean add(Chicken item) {
		data.put(item.getId(), item);
		return true;
	}
	
	public Chicken get(int id) {
		if(data.containsKey(id)) {
			return data.get(id);	
		}else {
			return null;
		}
		
		
	}

	@Override
	public List<Chicken> getAll() {
		List<Chicken> items = 
				new ArrayList<Chicken>();
		for(Chicken item : data.values()) {
			items.add(item);
		}
		return items;
	}

}
