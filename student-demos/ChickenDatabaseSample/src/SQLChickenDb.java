import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SQLChickenDb 
implements IChickenRepository
{
	private static final String TABLE_NAME = "chickens";
	private Connection con;
	private boolean isConnected;
	
	public SQLChickenDb() {
	  con = null;
	  initConnection();
	}
	
	public boolean initConnection() {
		try {
			Class.forName("org.sqlite.JDBC").newInstance();
			String url="jdbc:sqlite:chickendb.sqlite";
			con = DriverManager.getConnection(url);
			ensureTablesExist();
			isConnected = true;
			return true;
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		isConnected = false;
		return false;
	}

	private void ensureTablesExist()
	throws SQLException
	{
		String sql = "CREATE TABLE IF NOT EXISTS "
	                 +TABLE_NAME+
	                 "(id int, brand varchar(40), weight float, quality varchar(2) );";
		Statement st = con.createStatement();
		if(st.execute(sql)) {
			System.out.println("Table Modifications made");
		}else {
			System.out.println("No table modifications necessary");
		}
		
	}

	@Override
	public boolean add(Chicken item) {
		try {
			String sql="INSERT INTO "+TABLE_NAME+" (id, brand, quality, weight) VALUES(?,?,?,?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, item.getId());
			ps.setString(2, item.getBrand());
			ps.setFloat(3,item.getQuality());
			ps.setString(3, item.getQuality()+"");
			return ps.executeUpdate() > 0;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<Chicken> getAll() {
		String sql = "SELECT * FROM "+TABLE_NAME;
		List<Chicken> items = new ArrayList<Chicken>();
		try {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			if(rs != null) {
				while(rs.next()) {
					Chicken item = new Chicken( 
							rs.getInt(1),
							rs.getString(2),
							rs.getFloat(3),
							rs.getString(4).charAt(0)
							);
					items.add(item);
				}
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return items;
	}

}
