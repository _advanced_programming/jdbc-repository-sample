
public class Driver {
	
	public static void main(String[] args) {
		IMenuItemRepository db1 
		  //= new InMemoryMenuItemDb();
		  = new SQLMenuItemDb();
		
		System.out.println("----- In Mmory Db-----");
		testRepo(db1);
		
	}
	
	public static void testRepo(IMenuItemRepository repo) {
		System.out.println("Testing : "+repo.getClass().getName());
		System.out.println("Adding items to database");
		for(int i=0;i<3;i++) {
			repo.add(new MenuItem(i+1,"Item "+(i+1),(i+1)*500));
		}
		System.out.println("Retrieving second item");
		if(repo instanceof InMemoryMenuItemDb) {
			InMemoryMenuItemDb repoDb = (InMemoryMenuItemDb)repo;
			MenuItem itemRetrieved = repoDb.get(2);
			if(itemRetrieved == null) {
				System.out.println("Did not get second item");
			}else {
				System.out.println("Retrieved 2nd item "+itemRetrieved);
			}
		}
		System.out.println("printing all items");
		for(MenuItem item : repo.getAll()) {
			System.out.println(item);
		}
		
	}

}
