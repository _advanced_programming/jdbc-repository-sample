package lab6.inventory.controllers;

import java.util.List;

import lab6.inventory.database.MarkerSQLProvider;
import lab6.inventory.models.Marker;
import lab6.inventory.viewmodels.MarkerVM;

public class InventoryController {
	
	private MarkerSQLProvider markerSQLProvider;
    
    public InventoryController(){
    	markerSQLProvider = new MarkerSQLProvider();
    }
    
    public List<Marker> retrieveAllMarkers(){
    	return markerSQLProvider.selectAll();
    }

	public int deleteMultipleMarkers(List<MarkerVM> selectedMarkers) {
		int ids[] = new int[selectedMarkers.size()];
		for(int i=0;i<selectedMarkers.size();i++){
			ids[i] = selectedMarkers.get(i).getId();
		}
		return markerSQLProvider.deleteMultiple(ids);
	}
    

    

}
