import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InMemoryMenuItemDb
implements IMenuItemRepository
{
	private Map<Integer,MenuItem> data;
	
	public InMemoryMenuItemDb() {
		data = new HashMap<Integer,MenuItem>();
	}

	@Override
	public boolean add(MenuItem item) {
		data.put(item.getId(), item);
		return true;
	}
	
	public MenuItem get(int id) {
		if(data.containsKey(id))
		    return data.get(id);
		else
			return null;
	}

	@Override
	public List<MenuItem> getAll() {
		List<MenuItem> items = new ArrayList<MenuItem>();
		for(MenuItem item : data.values()) {
			items.add(item);
		}
		return items;
		
	}

}
