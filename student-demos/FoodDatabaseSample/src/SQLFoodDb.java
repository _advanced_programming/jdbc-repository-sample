import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SQLFoodDb 
implements IFoodRepository
{
	private static final String TABLE_NAME = "food";
	private Connection con;
	private boolean isConnected;
	
	public SQLFoodDb() {
		con = null;
	    initConnection();
	}
	
	private boolean initConnection() {
		try {
			Class.forName("org.sqlite.JDBC").newInstance();
			String url="jdbc:sqlite:food.sqlite";
			con = DriverManager.getConnection(url);
			ensureTablesExist();
			isConnected = true;
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		isConnected = false;
		return isConnected;
		
	}

	private void ensureTablesExist() 
	  throws SQLException
	{
		String sql = "CREATE TABLE IF NOT EXISTS "+
	                 TABLE_NAME+ "(id int, name varchar (50), price float)";
	 
		Statement st = con.createStatement();
		if(st.execute(sql)) {
			System.out.println("Database updated");
		}else {
			System.out.println("No database modifications necessary");
		}
	}

	@Override
	public boolean add(Food item) {
		try {
			String sql="INSERT INTO "+TABLE_NAME+" (id,name,price) VALUES (?,?,?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1,item.getId());
			ps.setString(2, item.getName());
			ps.setFloat(3, item.getCost());
			return ps.executeUpdate()>0;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<Food> getAll() {
		List<Food> items = new ArrayList<Food>();
		try {
			Statement st = con.createStatement();
			String sql = "SELECT * from "+TABLE_NAME;
			ResultSet rs = st.executeQuery(sql);
			if(rs !=null) {
				while(rs.next()) {
					items.add(
							new Food( 
									rs.getInt(1),
									rs.getString(2),
									rs.getFloat(3)
									)
							);
				}
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return items;
	}

}
