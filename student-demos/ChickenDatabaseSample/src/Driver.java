import java.util.List;

public class Driver {
	public static void main(String[] args) {
		IChickenRepository db1 = 
				//new InMemoryChickenDb();
				new SQLChickenDb();
		runTests(db1);
	}
	
	public static void runTests(
			IChickenRepository repo
			) {
		System.out.print("Class name : ");
		System.out.println(repo.getClass().getName());
		for(int i=1;i<=3;i++) {
			Chicken chick = new Chicken(i,"Brand "+i,(float)i, (char)i );
			repo.add(chick);
		}
		if(repo instanceof InMemoryChickenDb) {
			InMemoryChickenDb repoDb = 
					(InMemoryChickenDb)repo;
			Chicken itemRetrieved = repoDb.get(2);	
			System.out.println("Checking for item with id 2");
			if(itemRetrieved == null) {
				System.out.println("Item not found");
			}else {
				System.out.print("Found :"+itemRetrieved);
			}
		}
		System.out.println("retrieving items from repo");
		for(Chicken item : repo.getAll()) {
			System.out.println(item);
		}
		
	}

}
