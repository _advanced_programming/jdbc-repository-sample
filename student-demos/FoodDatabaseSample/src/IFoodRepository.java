import java.util.List;

public interface IFoodRepository {
   boolean add(Food item);
   List<Food> getAll();
}
