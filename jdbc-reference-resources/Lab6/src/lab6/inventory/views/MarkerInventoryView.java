package lab6.inventory.views;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import lab6.inventory.controllers.InventoryController;
import lab6.inventory.models.Marker;
import lab6.inventory.views.components.MarkerTable;

public class MarkerInventoryView extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	private InventoryController controller;
	
	private MarkerTable tblInventory;
	private JButton btnRefreshListings, btnDelete, btnUpdate, btnAdd;
	
	private List<Marker> markers;
	
	public MarkerInventoryView(InventoryController controller){
		this.controller = controller;
		markers = new ArrayList<Marker>();
		configureView();
	}

	private void configureView() {
		setTitle("Marker Inventory");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		
		btnRefreshListings = new JButton("Refresh Listings");
		btnRefreshListings.addActionListener(this);
		
		btnDelete = new JButton("Delete Selected");
		btnDelete.addActionListener(this);
		
		btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(this);
		
		btnAdd = new JButton("Add");
		btnAdd.addActionListener(this);
		
		JPanel pnlActions = new JPanel();
		pnlActions.add(btnAdd);
		pnlActions.add(btnUpdate);
		pnlActions.add(btnDelete);
		pnlActions.add(btnRefreshListings);
		add(pnlActions,BorderLayout.SOUTH);
		
		
		//---------Setting up table
		
		//create table with no data
		//NB. Custom Table and TableModel was created
		tblInventory = new  MarkerTable();
		
		//add table to scroll pane and adjust accordingly
		JScrollPane tblScrollPane = new JScrollPane(tblInventory);
		tblInventory.setFillsViewportHeight(true);
		
		add(tblInventory.getTableHeader(),BorderLayout.NORTH);
		add(tblScrollPane,BorderLayout.CENTER);
		
		
		//End setting up table
		setSize(500,300);
		setVisible(true);
		
		
	}

	@Override
	public void actionPerformed(ActionEvent ev) {
		if(ev.getSource().equals(btnRefreshListings)){
			actionRefreshData();
			
		}
		
		if(ev.getSource().equals(btnDelete)){
			actionDeleteSelected();
		}
		
	}
	
	private void actionRefreshData() {
		this.markers = controller.retrieveAllMarkers();
		tblInventory.refreshData(markers);
		
	}

	private void actionDeleteSelected(){
		if(tblInventory.isAnySelected() == false){
			showMessageDialog("Please select markers to be removed first");
			return;
		}
		int amountDeleted = controller.deleteMultipleMarkers(tblInventory.getSelectedMarkers());
		if(amountDeleted > 0){
			//refresh data
			actionRefreshData();
			showMessageDialog(amountDeleted+" Marker(s) Removed Successfully");
		}else{
			showMessageDialog("We were unable to remove your markers at this time");
		}				
	}
	
	private void showMessageDialog(String message){
		JOptionPane.showMessageDialog(this, message);
	}
	


}
