package lab6.inventory.views.components;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import lab6.inventory.models.Marker;
import lab6.inventory.viewmodels.MarkerVM;

public class MarkerTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	private List<MarkerVM> markers;
	public static final String[] COLUMN_NAMES = { "Id", "Name", "Type",
			"Checked" };

	public MarkerTableModel() {
		this(new ArrayList<Marker>());
	}

	public MarkerTableModel(List<Marker> markerData) {
		markers = new ArrayList<MarkerVM>();
		for (Marker m : markerData) {
			markers.add(new MarkerVM(m));
		}
	}
	
	public List<MarkerVM> getMarkers(){
		return markers;
	}
	
	public List<MarkerVM> getSelectedMarkers(){
		List<MarkerVM> selected = new ArrayList<MarkerVM>();
		for(MarkerVM m : markers){
			if(m.isSelected())
				selected.add(m);
		}
		return selected;
	}
	
	public boolean isAnySelected(){
		return getSelectedMarkers().size() > 0;
	}

	@Override
	public int getColumnCount() {
		return COLUMN_NAMES.length;
	}

	@Override
	public int getRowCount() {
		return markers.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO: Check Size constraints before executing code
		return markers.get(rowIndex).toObjectArray()[columnIndex];
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		if (col == 1 || col == 3)
			return true;
		else
			return false;
	}

	@Override
	public void setValueAt(Object value, int row, int col) {
		// update value at particular row and col
		// TODO: Check Size constraints before executing code
		markers.get(row).setValueAtIndex(col, value);
		fireTableCellUpdated(row, col);
	}

	/*
	 * JTable uses this method to determine the default renderer/ editor for
	 * each cell. If we didn't implement this method, then the last column would
	 * contain text ("true"/"false"), rather than a check box.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

}
