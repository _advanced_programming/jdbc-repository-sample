import java.util.List;

public interface IMenuItemRepository {
	boolean add(MenuItem item);
	
	List<MenuItem> getAll();

}
