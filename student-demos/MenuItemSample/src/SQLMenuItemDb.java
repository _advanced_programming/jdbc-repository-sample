import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SQLMenuItemDb
implements IMenuItemRepository
{
	private Connection con;
	private boolean isConnected;
	private static String TABLE_NAME="menuitems";
	
	public SQLMenuItemDb() {
		con = null;
		initConnection();
	}
	
	public boolean isConnected() {
		return isConnected;
	}
	
	public boolean initConnection() {
		try {
			Class.forName("org.sqlite.JDBC").newInstance();
			String url = "jdbc:sqlite:menuitems.sqlite";
			con = DriverManager.getConnection(url);
			isConnected = true;
			ensureDatabaseExists();
			return isConnected;
			
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isConnected = false;
		
		
	}

	private void ensureDatabaseExists()
	  throws SQLException
	{
		Statement st = con.createStatement();
		String sql = "CREATE TABLE IF NOT EXISTS "+
		             TABLE_NAME + "(id int, name varchar(50), cost float);";
		if(st.execute(sql)) {
			System.out.println("Modified schema");
		}else {
			System.out.println("No modifications necessary");
		}
	}

	@Override
	public boolean add(MenuItem item) {
		try {
			String sql = "INSERT INTO "+TABLE_NAME+
					     "(id,name,cost) values (?,?,?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, item.getId());
			ps.setString(2, item.getName());
			ps.setFloat(3, item.getCost());
			return ps.executeUpdate() > 0;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<MenuItem> getAll() {
		List<MenuItem> items = new ArrayList<MenuItem>();
		try {
			String sql = "SELECT * from "+TABLE_NAME;
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				while(rs.next()) {
			        items.add(new MenuItem(
			            rs.getInt("id"),
			            rs.getString("name"),
			            rs.getFloat(3)
			        		)
			        );		
				}
			}
			
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return items;
	}

}
