# JDBC and InMemory Repository Samples

@author ggordon

## Overview

Reference Resources using the JDBC API, Apache Log4j Loggers, MVC and Native GUI are all available as Eclipse Projects in the `jdbc-reference-resources` folder.

Sample eclipse projects created for each student demonstration are stored in `student-demos`.
