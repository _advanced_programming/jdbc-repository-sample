
public class Driver {
	public static void main(String[] args) {
		IFoodRepository db1 = 
				//new InMemoryFoodDb();
				new SQLFoodDb();
		runTests(db1);
	}
	
	public static void runTests(IFoodRepository repo) {
		System.out.print("Running Tests for ");
		System.out.println(repo.getClass().getName());
		System.out.println("Adding Items");
		for(int i=1;i<=3;i++) {
			repo.add(new Food(i,"Food "+i,i*500));
		}
		if(repo instanceof InMemoryFoodDb) {
		    InMemoryFoodDb repoDb = 
		    		(InMemoryFoodDb)repo;
			System.out.println("Getting food with id 2");
		    Food itemRetrieved = repoDb.get(2);
		    if(itemRetrieved == null) {
		    	System.out.println("Record not found");
		    }else {
		    	System.out.println("Found record with id 2");
		    	System.out.println(itemRetrieved);
		    }
			
		}
		System.out.println("Printing all records");
		for(Food item : repo.getAll()) {
			System.out.println(item);
		}
	}

}
