
public class Chicken {
	private int id;
	private String brand;
	private float weight;
	private char quality;
	public Chicken(int id, String brand, float weight, char quality) {
		super();
		this.id = id;
		this.brand = brand;
		this.weight = weight;
		this.quality = quality;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public float getWeight() {
		return weight;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
	public char getQuality() {
		return quality;
	}
	public void setQuality(char quality) {
		this.quality = quality;
	}
	public Chicken() {
		super();
	}
	@Override
	public String toString() {
		return "Chicken [id=" + id + ", brand=" + brand + ", weight=" + weight + ", quality=" + quality + "]";
	}
	
	

}
